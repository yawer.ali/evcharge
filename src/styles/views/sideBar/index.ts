import styled from "styled-components";

export const SideBarMainWrapper = styled.div`
  width: 100%;
  max-width: 13rem;
  min-height: 100vh;
  font-size: 2rem;
  font-weight: 500;
  color: #777;
  position: fixed;
  background-color: #2c3e50;
`;

export const LogoWrapper = styled.div`
  width: 100%;
  max-width: 10rem;
`;

export const Logo = styled.img`
  width: 100%;
  max-width: 6rem;
  padding: 1.5rem 2.5rem;
  border-radius: 50%;
  :hover {
    cursor: pointer;
    transform: scale(1.2);
  }
`;

export const MenuLinksWrapper = styled.div`
  width: 100%;
  max-width: 10rem;
`;

export const MenuLinks = styled.div`
  width: 100%;
  max-width: 10rem;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;

  .links {
    width: 100%;
    max-width: 8rem;
    padding-bottom: 1rem;
    color: white;
    text-decoration: none;
    :hover {
      cursor: pointer;
      color: rgb(0, 208, 130);
      transform: scale(1.1);
    }
  }
`;
