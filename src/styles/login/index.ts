import styled from "styled-components";

export const MainWrapper = styled.div`
  min-height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const LoginWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  gap: 3rem;
  border: 2px solid grey;
  padding: 10px;
`;
export const LogoWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;
export const Logo = styled.img`
  width: 100%;
  max-width: 4rem;
`;
export const SignIn = styled.div`
  font-size: 1.2rem;
  font-weight: 600;
`;

export const InputAndBtnWrapper = styled.div`
  /* border: 2px solid green; */
  width: 100%;
  max-width: 20vw;
`;
export const InputWrapper = styled.div``;
export const Input = styled.input`
  border: 0;
  border-bottom: 0.1rem solid #b9bdc5;
  width: 100%;
  max-width: 20rem;
  margin-bottom: 3.5rem;
  :focus {
    outline: none;
    border-bottom: 0.1rem solid #00a562;
  }
`;

export const Button = styled.button`
  color: #fff;
  font-size: 1rem;
  font-weight: 500;
  background: #00a562;
  border: 1px solid #00a562;
  border-radius: 0.1vw;
  padding: 0.6rem 2rem;
  width: 20.5rem;
  margin-bottom: 2rem;
  :hover {
    background-color: #00c173;
    cursor: pointer;
  }
`;
