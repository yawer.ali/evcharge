import {
  Logo,
  LogoWrapper,
  MenuLinks,
  MenuLinksWrapper,
  SideBarMainWrapper,
} from "styles/views/sideBar";
import logo from "assets/evlogo.jpeg";

import { Link } from "react-router-dom";

const Sidebar = () => {
  return (
    <SideBarMainWrapper>
      <MenuLinksWrapper>
        <LogoWrapper>
          <Link to="/dashboard">
            <Logo src={logo} alt="logo" />
          </Link>
        </LogoWrapper>

        <MenuLinks>
          <Link to="/dashboard" className="links">
            Dashboard
          </Link>
          <Link to="/Admin" className="links">
            Admin
          </Link>
          <Link to="/dashboard" className="links">
            Admin
          </Link>
          <Link to="/dashboard" className="links">
            Admin
          </Link>
          <Link to="/dashboard" className="links">
            Admin
          </Link>
        </MenuLinks>
      </MenuLinksWrapper>
    </SideBarMainWrapper>
  );
};

export default Sidebar;
